import React from 'react';
import './formcomponent.css';

const Form = (props) => {
    return (
        <div className="form">
            <input type="text"
                   className="formInput"
                   onChange={(e) => props.value(e.target.value)}
            />
            <button onClick={props.addMovie}
                    type="button"
                    className="formButton"
            >
                Add
            </button>
        </div>
    );
};

export default Form;