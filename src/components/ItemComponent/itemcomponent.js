import React, {Component} from 'react';
import './itemcomponent.css';

class ItemComponent extends Component {

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return nextProps.name !== this.props.name
    }

    render() {
        return (
            <div  className="newItem">
                <input type="text"
                       className="itemMovie"
                       value={this.props.name}
                       onChange={this.props.editMovie}

                />
                <button onClick={(name) => this.props.deleteMovie(this.props.name)}
                        className="newMovieButton"
                >X</button>
            </div>
        );
    }
};

export default ItemComponent;