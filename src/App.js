import React, { Component } from 'react';
import './App.css';
import Form from './components/FormComponent/formcomponent';
import ItemComponent from './components/ItemComponent/itemcomponent';

class App extends Component {

    state = {
        value: '',
        movies: []
    };


    valueInput = (value) =>{
        this.setState({value})
    };


    editMovie = (event, id) => {
        let movies = this.state.movies;
        movies[id].name = event;
        this.setState({movies})
    };


    deleteMovie = (name) => {
        const movies = this.state.movies;
        const index = movies.findIndex((movie) => movie.name === name)
        movies.splice(index, 1)
        this.setState({
            movies
        })
    };


    addMovie = () => {
        const movies = this.state.movies;
        const newMovie = {name: this.state.value};
        movies.push(newMovie);
        this.setState({
            movies
        });
    };

  render() {
    return (
      <div className="App">
          <h1>My movie collection</h1>
        <Form value={this.valueInput} addMovie={this.addMovie}/>
          {this.state.movies.map((movie, index) => {
              return (
                  <ItemComponent
                    key={index}
                    name={movie.name}
                    deleteMovie={this.deleteMovie}
                    editMovie={(event) => this.editMovie(event.target.value, index)}
                  />
              )
          })}

      </div>
    );
  }
}

export default App;
